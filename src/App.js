import React from 'react';
import './App.css';

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<h1>My First React-Electron App</h1>
				<p>This is my first Electron App which uses React for the view engine.</p>
			</header>
		</div>
	);
}

export default App;
